class CreatePrescriptions < ActiveRecord::Migration
  def change
    create_table :prescriptions do |t|
      t.string :name
      t.string :instructions
      t.references :patient, index: true

      t.timestamps
    end
  end
end

Rails.application.routes.draw do
  resources :prescriptions

  resources :patients

  root 'patients#index'

end
